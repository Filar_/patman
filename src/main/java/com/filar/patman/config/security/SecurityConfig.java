package com.filar.patman.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Created by kamil on 08.03.16.
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{
	@Autowired
	UserService userService;
	@Autowired
	PasswordEncoder passwordEncoder;
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userService).passwordEncoder(passwordEncoder);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.
				formLogin().loginPage("/login").and().
				rememberMe().tokenValiditySeconds(2419200).key("PatmanKey").and().
				logout().logoutSuccessUrl("/").and().

				authorizeRequests()
				.antMatchers("/").permitAll()
				.antMatchers("/login").not().authenticated()
				.antMatchers("/notfound").permitAll()
				.antMatchers("/member/register").not().authenticated()
				.antMatchers("/member/show/*").permitAll()
				.antMatchers("/member/list").permitAll()
				.antMatchers("/task/show/*").permitAll()
				.antMatchers("/task/new").authenticated()
				.antMatchers("/task/comment/edit/*").hasAnyAuthority("MANAGE", "ADMIN")
				.antMatchers("/task/comment/delete/*").hasAnyAuthority("MANAGE", "ADMIN")
				.antMatchers("/task/comment/new").authenticated()
				.antMatchers("/task/assign").authenticated()
				.antMatchers("/task/dissociate").authenticated()
				.antMatchers("/task/edit/*").authenticated()
				.antMatchers("/task/changestatus").authenticated()
				.antMatchers("/project/list").permitAll()
				.antMatchers("/project/show/*").permitAll()
				.antMatchers("/project/new").access("isAuthenticated() and (hasAuthority('MANAGE') or hasAuthority('ADMIN'))")
				.antMatchers("/project/edit/*").access("isAuthenticated() and (hasAuthority('MANAGE') or hasAuthority('ADMIN'))")
				.antMatchers(HttpMethod.POST, "/task/comment/new").authenticated()
				.anyRequest().hasRole("ADMIN");
	}
}
