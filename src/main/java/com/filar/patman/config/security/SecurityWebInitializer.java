package com.filar.patman.config.security;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Created by kamil on 08.03.16.
 */
public class SecurityWebInitializer extends AbstractSecurityWebApplicationInitializer{
}
