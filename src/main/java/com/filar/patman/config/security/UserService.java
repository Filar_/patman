package com.filar.patman.config.security;

import com.filar.patman.data.entities.Member;
import com.filar.patman.data.service.MemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kamil on 08.03.16.
 */
@Component
public class UserService implements UserDetailsService {
	@Autowired
	MemberRepository memberRepository;
	UserService(){
	}

	@Override
	public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
		Member member = memberRepository.findByEmail(s.toLowerCase());
		if(member != null){
			List<GrantedAuthority> authorities = new ArrayList<>();
			member.getAuthorities().stream().forEach(
					(a)-> authorities.add(new SimpleGrantedAuthority(a.name()))
			);
			return new User(member.getEmail(), member.getPassword(), authorities);
		} else {
			throw new UsernameNotFoundException("User with e-mail:" + s + "not found.");
		}
	}
}
