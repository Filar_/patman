package com.filar.patman.data.entities;

import com.filar.patman.data.entities.task.Task;
import com.filar.patman.data.helpers.Authority;
import com.filar.patman.data.helpers.TaskStatus;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by kamil on 23.02.16.
 */
@Entity
public class Member {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@NotNull
	@Size(min=2, max=50, message = "First name must be between 2 and 50 characters long")
	private String firstName;
	@NotNull
	@Size(min=2, max=50, message = "Surname must be between 2 and 50 characters long")
	private String surname;
	@NotNull
	@Size(min=4, max=200, message = "Password must be at least 4 characters long")
	private String password;
	@NotNull
	@Column(unique = true)
	private String email;
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(
			name = "Member_Task",
			joinColumns = @JoinColumn(name = "Member_Id", referencedColumnName = "id"),
			inverseJoinColumns = @JoinColumn(name = "Task_Id", referencedColumnName = "id")
	)
	private Set<Task> assignedTasks = new HashSet<>();
	@ManyToMany(fetch=FetchType.EAGER, mappedBy = "managers")
	private Set<Project> managedProjects = new HashSet<>();
	@ElementCollection(fetch = FetchType.EAGER)
	@Enumerated
	private Set<Authority> authorities = new HashSet<>();

	public Member() {
		super();
	}

	public Member(Long id, String firstName, String surname, String password, String email,
				  Set<Task> assignedTasks, Set<Project> managedProjects, Set<Authority> authorities) {
		this(id, firstName, surname, password, email);
		this.assignedTasks = assignedTasks;
		this.managedProjects = managedProjects;
		this.authorities = authorities;
	}
	public Member(Long id, String firstName, String surname, String password, String email){
		this.id = id;
		this.firstName = firstName;
		this.surname = surname;
		this.password = password;
		this.email = email;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Set<Task> getAssignedTasks() {
		return assignedTasks;
	}

	public void setAssignedTasks(Set<Task> assignedTasks) {
		this.assignedTasks = assignedTasks;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Set<Project> getManagedProjects() {
		return managedProjects;
	}

	public void setManagedProjects(Set<Project> managedProjects) {
		this.managedProjects = managedProjects;
	}

	public Set<Authority> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(Set<Authority> authorities) {
		this.authorities = authorities;
	}

	public void addManagedProject(Project project){
		managedProjects.add(project);
	}

	public void deleteManagedProject(Project project){
		managedProjects.remove(project);
	}

	public String getFullName(){ return firstName +" "+ surname; }

	public void assignTask(Task task) {
		assignedTasks.add(task);
	}

	public void removeTask(Task task) {
		assignedTasks.remove(task);
	}


	// get assigned tasks that have the specified status
	public Set<Task> getTasksWithStatus(TaskStatus status) {
		return new HashSet<Task>(
				assignedTasks.stream().filter(
						(i)->i.getStatus()==status
				).collect( Collectors.toList() )
		);
	}

	public int getNumberOfActiveTasks(){
		return getTasksWithStatus(TaskStatus.IN_PROGRESS).size()+getTasksWithStatus(TaskStatus.NEW).size();
	}



	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(email).toHashCode();
	}

	@Override
	public boolean equals(Object m) {
		if(m.getClass()!=this.getClass()) return false;
		if ( ((Member)m ).getEmail().equals(email) ) return true;
		return false;
	}
}
