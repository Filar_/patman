package com.filar.patman.data.entities;

import com.filar.patman.data.entities.task.Task;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by kamil on 23.02.16.
 */
@Entity
public class Project {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@NotNull
	@Size(min=3, max=50, message="The name must be between 3 and 50 letters long!")
	@Column(unique = true)
	private String name;
	@OneToMany(fetch=FetchType.EAGER, mappedBy="project", orphanRemoval = true, cascade = CascadeType.ALL)
	private Set<Task> tasks = new HashSet<>();

	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(
			name="Member_Project",
			inverseJoinColumns = @JoinColumn(name="Member_Id", referencedColumnName = "id"),
			joinColumns = @JoinColumn(name="Project_Id", referencedColumnName = "id")
	)
	private Set<Member> managers = new HashSet<>();

	public Project() {
	}


	public Project(Long id, String name, Set<Task> tasks, Set<Member> managers) {
		this(id,name);
		this.tasks = tasks;
		this.managers = managers;
	}

	public Project(Long id, String name) {
		this.id = id;
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Task> getTasks() {
		return tasks;
	}

	public void setTasks(Set<Task> tasks) {
		this.tasks = tasks;
	}

	public Set<Member> getManagers() {
		return managers;
	}


	public void setManagers(Set<Member> managers) {
		this.managers = managers;
	}

	public void addManager(Member manager){
		managers.add(manager);
	}

	public void deleteManager(Member manager){
		managers.remove(manager);
	}

	public void addTask(Task task) {
		tasks.add(task);
	}
	public void deleteTask(Task task){
		tasks.remove(task);
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(name).toHashCode();
	}

	@Override
	public boolean equals(Object o) {
		if(o.getClass() != this.getClass()) return false;
		if ( ((Project) o).getName().equals(name) ) return true;
		return false;
	}

	public boolean hasManager(String email){
		return managers.stream().anyMatch((i)->i.getEmail().equals(email));
	}
}
