package com.filar.patman.data.entities.task;

import com.filar.patman.data.entities.Member;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * Created by kamil on 10.03.16.
 */
@Entity
@DiscriminatorValue("Comment")
public class Comment extends Event {

	@NotNull
	@Size(min=2, max=255, message = "Comment must be between 2 and 255 characters long")
	private String content;


	public Comment() {
		super.setEditableType(new CommentEditor(this));
	}

	public Comment(Long id, Date date, Member author, String content, Task task) {
		super(id, date, author, task);
		this.content = content;
		setEditableType(new CommentEditor(this));
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public boolean isEditable() { return true; }

	@Override
	public boolean equals(Object o) {
		if (o == null) return false;
		if (!o.getClass().equals(this.getClass())) return false;
		Comment second = (Comment) o;
		if (getAuthor() == null && getAuthor()!=second.getAuthor()) return false;
		if (!this.getDate().equals(second.getDate())) return false;
		if ( getAuthor() == null ){
			if(second.getAuthor() != null)
				return false;
		} else {
			if (!this.getAuthor().equals(second.getAuthor())) return false;
		}

		if (!this.getContent().equals(second.getContent())) return false;
		return true;
	}

	@Override
	public int hashCode() {
		if (getAuthor() == null) return new HashCodeBuilder().append(getDate()).append(getContent()).toHashCode();
		return new HashCodeBuilder().append(getDate()).append(getAuthor()).append(getContent()).toHashCode();
	}
}
