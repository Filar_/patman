package com.filar.patman.data.entities.task;

/**
 * Created by kamil on 24.03.16.
 */
public interface Editor {
	boolean isEditable();
	void edit(String newContent);
}

class NotEditable implements Editor {
	@Override
	public boolean isEditable() {
		return false;
	}

	@Override
	public void edit(String newContent) {
		throw new IllegalStateException();
	}
}

class CommentEditor implements Editor {
	private Comment comment;
	public CommentEditor(Comment comment){
		this.comment = comment;
	}

	@Override
	public boolean isEditable() {
		return true;
	}

	@Override
	public void edit(String newContent) {
		comment.setContent(newContent);
	}
}
