package com.filar.patman.data.entities.task;

import com.filar.patman.data.entities.Member;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by kamil on 16.03.16.
 */

@Entity
@Inheritance
@DiscriminatorColumn(name = "EVENT_TYPE")
@Table(name="Event")

public abstract class Event implements Comparable<Event> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private Date date = new Date();

	@ManyToOne
	@JoinTable(
			name="Member_Event",
			joinColumns = @JoinColumn(name = "Event_Id", referencedColumnName = "id"),
			inverseJoinColumns = @JoinColumn(name = "Member_Id", referencedColumnName = "id")
	)
	private Member author;

	@ManyToOne
	@JoinTable(
			name="Task_Event",
			joinColumns = @JoinColumn(name = "Event_Id", referencedColumnName = "id"),
			inverseJoinColumns = @JoinColumn(name = "Task_Id", referencedColumnName = "id")
	)
	private Task task;

	@Transient
	private Editor editableType;

	public Event() {
	}

	public Event(Long id, Date date, Member author, Task task) {
		this.id = id;
		this.date = date;
		this.author = author;
		this.task = task;
	}

	abstract public String getContent();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date= date;
	}

	public Member getAuthor() {
		return author;
	}

	public void setAuthor(Member author) {
		this.author = author;
	}


	public Task getTask() {
		return task;
	}

	public void setTask(Task task) {
		this.task = task;
	}

	@Override
	public int compareTo(Event event) {
		return date.compareTo(event.getDate());
	}

	public boolean isEditable(){
		return editableType.isEditable();
	}

	public void edit(String newContent){
		editableType.edit(newContent);
	}

	void setEditableType(Editor editableType) {
		this.editableType = editableType;
	}
}
