package com.filar.patman.data.entities.task;

import com.filar.patman.data.helpers.TaskStatus;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Created by kamil on 16.03.16.
 */
@Entity
@DiscriminatorValue("StatusChange")
public class StatusChange extends Event {

	private TaskStatus previous;
	private TaskStatus newStatus;

	public StatusChange() {
		setEditableType(new NotEditable());
	}

	public StatusChange(TaskStatus previous, TaskStatus newStatus) {
		this.previous = previous;
		this.newStatus = newStatus;
		setEditableType(new NotEditable());
	}

	@Override
	public String getContent() {
		return "Changed state: " + previous.getFriendlyName() +" -> " + newStatus.getFriendlyName();
	}

	public TaskStatus getPrevious() {
		return previous;
	}

	public void setPrevious(TaskStatus previous) {
		this.previous = previous;
	}

	public TaskStatus getNewStatus() {
		return newStatus;
	}

	public void setNewStatus(TaskStatus newStatus) {
		this.newStatus = newStatus;
	}

}
