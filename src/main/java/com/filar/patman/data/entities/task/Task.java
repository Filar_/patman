package com.filar.patman.data.entities.task;

import com.filar.patman.data.entities.Member;
import com.filar.patman.data.entities.Project;
import com.filar.patman.data.helpers.TaskStatus;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.SortComparator;
import org.hibernate.annotations.SortNatural;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.*;

/**
 * Created by kamil on 23.02.16.
 */
@Entity
public class Task {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@NotNull
	@Size(min=3, max = 50, message = "Task name must be between 3 and 50 characters long")
	private String name;

	@NotNull
	@Size(min = 5, message = "Content of a task must be longer than 5 characters")
	private String content;

	@Enumerated(EnumType.STRING)
	private TaskStatus status;

	@ManyToMany(fetch = FetchType.EAGER, mappedBy = "assignedTasks")
	private Set<Member> assignedTo = new HashSet<>();

	@ManyToOne()
	@JoinTable(
			name="Project_Task",
			joinColumns = @JoinColumn(name = "Task_id", referencedColumnName = "id"),
			inverseJoinColumns = @JoinColumn(name = "Project_id", referencedColumnName = "id")
	)
	private Project project;

	private Date dateCreated = new Date();
	@ManyToOne
	private Member author;

	@OneToMany(fetch=FetchType.EAGER, mappedBy = "task")
	@SortNatural
	private SortedSet<Event> events = new TreeSet<>();

	public Task() {
		super();
	}

	public Task(Long id, String name, String content, TaskStatus status, Project project, Member author ){
		this.id = id;
		this.name = name;
		this.content = content;
		this.status = status;
		this.author = author;
		this.project = project;
	}

	public Task(Long id, String name, String content, TaskStatus status, Project project,
				 Member author, Date dateCreated, SortedSet<Event> events, Set<Member> assignedTo) {
		this(id, name, content, status, project, author);
		this.assignedTo = assignedTo;
		this.dateCreated = dateCreated;
		this.events = events;
	}


	public String getContent() {
		return content;
	}

	public void setContent(String taskContents) {
		this.content = taskContents;
	}

	public TaskStatus getStatus() {
		return status;
	}

	public void setStatus(TaskStatus status) {
		this.status = status;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public Set<Member> getAssignedTo() {
		return assignedTo;
	}

	public void setAssignedTo(Set<Member> assignedTo) {
		this.assignedTo = assignedTo;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Member getAuthor() {
		return author;
	}

	public void setAuthor(Member author) {
		this.author = author;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public boolean equals(Object o){
		if ( o == null ) return false;
		if (this.getClass() != o.getClass()) return false;
		Task task = (Task) o;
		if (project == null || task.project == null) return false;
		if ( task.getName().equals(name) && project.getName().equals( task.project.getName() ) ) return true;
		return false;
	}

	@Override
	public int hashCode(){
		if ( project == null ) return new HashCodeBuilder().append(name).toHashCode();
		return new HashCodeBuilder().append(name).append(project.getName()).toHashCode();
	}

	public Set<Event> getEvents() {
		return events;
	}

	public void setEvents (SortedSet<Event> events){
		this.events = events;
	}

	public void addEvent(Event event){
		events.add(event);
	}

	public void assignMember(Member member){
		assignedTo.add(member);
	}

	public void dissociate(Member member){
		assignedTo.remove(member);
	}

	public boolean hasAssignedMember(String email) {
		return assignedTo.stream().anyMatch((i)->i.getEmail().equals(email));
	}
}


