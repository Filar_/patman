package com.filar.patman.data.helpers;

/**
 * Created by kamil on 23.02.16.
 */
public enum Authority {
	ADMIN("Administrator"),
	MANAGE("Manager");

	private final String friendlyName;

	Authority(String friendlyName){
		this.friendlyName = friendlyName;
	}

	public String getFriendlyName() {
		return friendlyName;
	}
}
