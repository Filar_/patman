package com.filar.patman.data.helpers;

/**
 * Created by kamil on 23.02.16.
 */
public enum TaskStatus {
	NEW("new"),
	IN_PROGRESS("in progress"),
	SOLVED("solved"),
	SUSPENDED("suspended");
	private final String friendlyName;

	TaskStatus(String friendlyName) {
		this.friendlyName = friendlyName;
	}

	public String getFriendlyName() {
		return friendlyName;
	}
}
