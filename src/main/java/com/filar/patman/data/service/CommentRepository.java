package com.filar.patman.data.service;

import com.filar.patman.data.entities.task.Comment;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by kamil on 10.03.16.
 */
public interface CommentRepository extends JpaRepository<Comment, Long> {
}
