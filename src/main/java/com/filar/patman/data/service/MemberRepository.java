package com.filar.patman.data.service;

import com.filar.patman.data.entities.Member;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by kamil on 01.03.16.
 */
public interface MemberRepository extends JpaRepository<Member,Long>{
	Member findByEmail(String email);
}
