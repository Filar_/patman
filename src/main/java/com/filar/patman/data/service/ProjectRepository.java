package com.filar.patman.data.service;

import com.filar.patman.data.entities.Project;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by kamil on 28.02.16.
 */
public interface ProjectRepository extends JpaRepository<Project, Long> {
}
