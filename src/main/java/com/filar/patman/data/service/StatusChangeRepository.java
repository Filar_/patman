package com.filar.patman.data.service;

import com.filar.patman.data.entities.task.StatusChange;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by kamil on 19.03.16.
 */
public interface StatusChangeRepository extends JpaRepository<StatusChange, Long> {
}
