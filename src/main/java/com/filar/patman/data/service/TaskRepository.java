package com.filar.patman.data.service;

import com.filar.patman.data.entities.task.Task;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by kamil on 23.02.16.
 */
public interface TaskRepository extends JpaRepository<Task,Long> {

}
