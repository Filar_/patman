package com.filar.patman.web;

import com.filar.patman.data.entities.Member;
import com.filar.patman.data.service.MemberRepository;
import com.filar.patman.web.helpers.Helpers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;

/**
 * Created by kamil on 18.02.16.
 */
@Controller
public class HomeController {
	MemberRepository memberRepository;

	@Autowired
	public HomeController(MemberRepository memberRepository) {
		this.memberRepository = memberRepository;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/")
	public String home (Principal principal, Model model){
		if(principal!=null) {
			Member member = memberRepository.findByEmail(principal.getName());
			Helpers.fillModelWithMembersTaskLists(model, member);
		}
		return "home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/notfound")
	public String notfound(@RequestParam("type") String type, @RequestParam("id") Long id, Model model){
		model.addAttribute("type", type);
		model.addAttribute("id", id);
		return "notfound";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/login")
	public String login(@RequestParam(value = "error", required = false) String hasError, Model model){
		if(hasError != null) model.addAttribute("hasError", true);
		return "login";
	}
}
