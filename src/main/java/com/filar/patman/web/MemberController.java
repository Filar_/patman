package com.filar.patman.web;

import com.filar.patman.data.entities.Member;
import com.filar.patman.data.helpers.Authority;
import com.filar.patman.data.service.MemberRepository;
import com.filar.patman.web.helpers.Helpers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by kamil on 29.02.16.
 */
@Controller
@RequestMapping("/member")
public class MemberController {
	@Autowired
	MemberRepository memberRepository;
	@Autowired
	PasswordEncoder passwordEncoder;

	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String register(Model model){

		model.addAttribute("newMember", new Member());
		model.addAttribute("passwordsMatch", true);
		return "member/register";
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String postRegister(@RequestParam("retypedPassword") String retypedPassword,
							   @Valid @ModelAttribute("newMember") Member newMember, Errors errors, Model model
	){

		if (!newMember.getPassword().equals(retypedPassword)){
			model.addAttribute("passwordsMatch", false);
			return "member/register";

		} else {
			if (errors.hasErrors()){
				model.addAttribute("passwordsMatch", true);
				return "member/register";
			} else {
				newMember.setPassword(passwordEncoder.encode(newMember.getPassword()));
				newMember.setEmail(newMember.getEmail().toLowerCase());
				// first user is an admin
				if(memberRepository.count()==0) newMember.getAuthorities().add(Authority.ADMIN);
				// e-mail must be unique, if it's not, save() will throw an exception, then the message is added to errors
				try{
					memberRepository.save(newMember);
				}catch(DataIntegrityViolationException e){
					errors.reject("org.springframework.dao.DataIntegrityViolationException","Member with entered e-mail already exists.");
					return "member/register";
				}
				return "redirect:show/" + newMember.getId() ;
			}
		}
	}

	@RequestMapping(value = "/show/{id}", method = RequestMethod.GET)
	public String showMember(Model model, @PathVariable Long id){
		Member member = memberRepository.findOne(id);
		if(member == null) return Helpers.notFoundRedirect(id, "member");

		Helpers.fillModelWithMembersTaskLists(model, member);
		model.addAttribute(member);
		return "member/show";
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public String memberList(Model model){
		model.addAttribute("members", memberRepository.findAll());
		return "member/list";
	}

}
