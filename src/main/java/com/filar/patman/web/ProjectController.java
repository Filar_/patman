package com.filar.patman.web;

import com.filar.patman.data.entities.Member;
import com.filar.patman.data.entities.Project;
import com.filar.patman.data.helpers.Authority;
import com.filar.patman.data.service.MemberRepository;
import com.filar.patman.data.service.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

/**
 * Created by kamil on 25.02.16.
 */
@Controller
@RequestMapping("/project")
public class ProjectController {
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	MemberRepository memberRepository;
	@RequestMapping(method = RequestMethod.GET, value = "/new")
	public String addProject(Model model){
		model.addAttribute(new Project());
		model.addAttribute("memberList", memberRepository.findAll());
		return "project/new";
	}

	@RequestMapping(method = RequestMethod.POST, value="/new")
	public String postProject(@RequestParam("managerId") Long id, @Valid Project project, Errors errors, Model model){

		if(!errors.hasErrors()) {
			// the project might not have any errors yet, but the uniqueness of the name hasn't been checked, so it is now
			try {
					Member member;
					member = memberRepository.findOne(id);
					addManagerToProjectAndSave(member, project);

			} catch (DataIntegrityViolationException e) {
				errors.reject("org.springframework.dao.DataIntegrityViolationException", "Project with such name already exists");
			}
		}

		// now the actual error handling
		if(errors.hasErrors()){
			model.addAttribute("memberList", memberRepository.findAll());
			model.addAttribute("managerId", id);
			return "project/new";
		}

		return "redirect:show/"+project.getId();
	}

	@RequestMapping(method = RequestMethod.GET, value="/show/{id}")
	public String showProject(@PathVariable Long id, Model model){
		Project project = projectRepository.findOne(id);
		if (project == null) return "redirect:/notfound?type=project&id="+id;
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (project.hasManager(auth.getName()) || auth.getAuthorities().contains(Authority.ADMIN) || auth.getAuthorities().contains(Authority.MANAGE)) {
			model.addAttribute("isManager", true);
		} else {
			model.addAttribute("isManager", false);
		}

		model.addAttribute("project", project);
		return "project/show";
	}

	@RequestMapping(method = RequestMethod.GET, value="/list")
	public String projectList(Model model){
		model.addAttribute("projects", projectRepository.findAll());
		return "project/list";
	}

	@RequestMapping(method = RequestMethod.GET, value="/edit/{id}")
	public String editProject(@PathVariable Long id, Model model){
		Project project = projectRepository.findOne(id);
		if (project == null) return "redirect:/notfound?type=project&id="+id;

		model.addAttribute("project", project);
		model.addAttribute("members", memberRepository.findAll());
		return "project/edit";
	}

	@RequestMapping(method = RequestMethod.POST, value ="/edit/changename")
	public String changeName(@Valid Project project, Errors errors, Model model){

		// first check for form validation errors, then the uniqueness of name will be checked while saving to db
		if(!errors.hasErrors()) {
			String newName = project.getName();
			project = projectRepository.findOne(project.getId());
			project.setName(newName);
			try {
				projectRepository.save(project);
			} catch (DataIntegrityViolationException e) {
				errors.reject("org.springframework.dao.DataIntegrityViolationException", "Project with such name already exists");
			}
		}


		if(errors.hasErrors()){
			model.addAttribute("members", memberRepository.findAll());
			// populate the project object with its managers, otherwise the collection would be empty
			project.setManagers(projectRepository.findOne(project.getId()).getManagers());
			return "project/edit";
		}

		return "redirect:/project/show/" + project.getId();
	}

	@RequestMapping(method = RequestMethod.POST, value="/edit/addmanager")
	public String addManager(@RequestParam("managerId") Long managerId, @RequestParam("projectId") Long projectId){
		addManagerToProjectAndSave(memberRepository.findOne(managerId), projectRepository.findOne(projectId));
		return "redirect:/project/show/" + projectId;
	}

	@RequestMapping(method = RequestMethod.POST, value="/edit/deletemanager")
	public String deleteManager(@RequestParam("managerId") Long managerId, @RequestParam("projectId") Long projectId){
		Member manager = memberRepository.findOne(managerId);
		Project project = projectRepository.findOne(projectId);

		project.deleteManager(manager);
		manager.deleteManagedProject(project);

		memberRepository.save(manager);
		projectRepository.save(project);

		return "redirect:/project/edit/" + projectId;
	}

	private void addManagerToProjectAndSave(Member manager, Project project){
		project.addManager(manager);
		manager.addManagedProject(project);

		projectRepository.save(project);
		memberRepository.save(manager);
	}
}
