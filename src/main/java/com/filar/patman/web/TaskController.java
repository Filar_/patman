package com.filar.patman.web;

import com.filar.patman.data.entities.Member;
import com.filar.patman.data.entities.Project;
import com.filar.patman.data.entities.task.StatusChange;
import com.filar.patman.data.entities.task.Task;
import com.filar.patman.data.entities.task.Comment;
import com.filar.patman.data.helpers.Authority;
import com.filar.patman.data.helpers.TaskStatus;
import com.filar.patman.data.service.*;
import com.filar.patman.web.helpers.AccessDeniedException;
import com.filar.patman.web.helpers.Helpers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Date;
import java.util.List;

/**
 * Created by kamil on 02.03.16.
 */
@Controller
@RequestMapping("/task")
public class TaskController {
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	TaskRepository taskRepository;
	@Autowired
	MemberRepository memberRepository;
	@Autowired
	CommentRepository commentRepository;
	@Autowired
	StatusChangeRepository statusChangeRepository;

	@RequestMapping(value="/new", method = RequestMethod.GET)
	public String newTask(Model model, @RequestParam(value = "projectId", required = true) Long projectId){
		Task task = new Task();
		model.addAttribute("newTask", task);
		model.addAttribute("projectId", projectId);
		return "task/new";
	}

	@RequestMapping(value="/new", method = RequestMethod.POST )
	public String postNewTask(Model model, @RequestParam("projectId") Long projectId,  @Valid @ModelAttribute("newTask") Task task, Errors errors){
		if(errors.hasErrors()){
			model.addAttribute("projectId", projectId);
			return "task/new";
		}
		Project project = projectRepository.findOne(projectId);
		if (projectId == null) return Helpers.notFoundRedirect(projectId, "project");

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Member author = memberRepository.findByEmail(auth.getName());

		if (!author.getManagedProjects().contains(project)) return "redirect:/project/show/" + projectId;

		task.setAuthor(author);
		task.setStatus(TaskStatus.NEW);
		task.setDateCreated(new Date());

		project.addTask(task);
		task.setProject(project);

		taskRepository.save(task);
		projectRepository.save(project);
		return "redirect:show/" + task.getId();
	}

	@RequestMapping(value="/show/{id}", method = RequestMethod.GET)
	public String showTask(@PathVariable Long id, Model model){
		Task task = taskRepository.findOne(id);
		if ( task == null ) return Helpers.notFoundRedirect(id, "task");


		model.addAttribute("newComment", new Comment());

		taskShowInit(model, task);

		return "task/show";
	}

	@RequestMapping(value="/comment/new", method=RequestMethod.POST)
	public String newComment(@Valid @ModelAttribute("newComment") Comment comment, Errors errors,
							 @RequestParam("taskId") Long taskId, Model model){

		Task task = taskRepository.findOne(taskId);
		if (task == null) return Helpers.notFoundRedirect(taskId, "task");

		if(errors.hasErrors()){
			model.addAttribute("newComment", comment);
			taskShowInit(model, task);
			return "task/show";
		}

		comment.setTask(task);
		task.addEvent(comment);

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		comment.setAuthor(memberRepository.findByEmail(auth.getName()));

		commentRepository.save(comment);
		taskRepository.save(task);
		return "redirect:/task/show/" + taskId;
	}

	@RequestMapping(value="/assign", method = RequestMethod.POST)
	public String assignMember(@RequestParam("memberId") Long memberId, @RequestParam("taskId") Long taskId ){

		Task task = taskRepository.findOne(taskId);
		validateTaskEditor(task);

		Member member = memberRepository.findOne(memberId);

		member.assignTask(task);
		task.assignMember(member);

		memberRepository.save(member);
		taskRepository.save(task);
		return "redirect:/task/show/"+task.getId();
	}

	@RequestMapping(value="/dissociate", method = RequestMethod.POST)
	public String dissociateMember(@RequestParam("memberId") Long memberId, @RequestParam("taskId") Long taskId){

		Task task = taskRepository.findOne(taskId);
		validateTaskEditor(task);
		Member member = memberRepository.findOne(memberId);

		task.dissociate(member);
		member.removeTask(task);

		memberRepository.save(member);
		taskRepository.save(task);
		return "redirect:/task/show/" + taskId;
	}

	@RequestMapping(value="/changestatus", method = RequestMethod.POST)
	public String changeStatus(@RequestParam("newStatus") String newStatus, @RequestParam("taskId") Long taskId,
							   Principal principal){
		Task task = taskRepository.findOne(taskId);

		validateTaskEditor(task);

		if(task.getStatus().equals(TaskStatus.valueOf(newStatus))) return "redirect:/task/show/" + taskId;

		StatusChange change = new StatusChange(task.getStatus(), TaskStatus.valueOf(newStatus));
		Member member = memberRepository.findByEmail(principal.getName());

		task.setStatus(TaskStatus.valueOf(newStatus));
		task.addEvent(change);
		change.setTask(task);
		change.setAuthor(member);

		statusChangeRepository.save(change);
		taskRepository.save(task);

		return "redirect:/task/show/" + taskId;
	}


	@RequestMapping(value="/edit/{id}", method = RequestMethod.GET)
	public String editTaskContent(@PathVariable("id") Long id, Model model){
		Task task = taskRepository.findOne(id);
		validateTaskEditor(task);

		if (task == null){
			return Helpers.notFoundRedirect(id, "Task");
		}
		model.addAttribute("task", task);
		return "task/editcontent";
	}

	@RequestMapping(value="/edit/{id}", method = RequestMethod.POST)
	public String editTaskContent(@Valid @ModelAttribute("task") Task task, Errors errors, Model model){
		if(errors.hasErrors()){
			model.addAttribute("task", task);
			return "task/editcontent";
		}

		Task newTask = taskRepository.findOne(task.getId());
		validateTaskEditor(task);
		if( newTask == null ){
			return Helpers.notFoundRedirect(task.getId(), "Task");
		}
		newTask.setContent(task.getContent());

		taskRepository.save(newTask);
		return "redirect:/task/show/" + task.getId();
	}

	@RequestMapping(value="/comment/edit/{id}", method = RequestMethod.GET)
	public String editComment(@PathVariable Long id, Model model){
		Comment comment = commentRepository.findOne(id);
		if ( comment == null ) return Helpers.notFoundRedirect(id, "comment");
		model.addAttribute("comment", comment);
		return "task/editcomment";
	}

	@RequestMapping(value="/comment/edit/{id}", method = RequestMethod.POST)
	public String postEditComment(@ModelAttribute @Valid Comment comment, Errors errors, Model model) {
		Comment newComment = commentRepository.findOne(comment.getId());
		if (newComment == null) return Helpers.notFoundRedirect(comment.getId(), "comment");
		if (errors.hasErrors()){
			model.addAttribute("comment", comment);
			return "task/editcomment";
		}
		newComment.setContent(comment.getContent());
		commentRepository.save(newComment);
		return "redirect:/task/show/" + newComment.getTask().getId();
	}

	@RequestMapping(value="/comment/delete/{id}", method = RequestMethod.POST)
	public String deleteComment(@PathVariable Long id){
		Comment comment = commentRepository.findOne(id);
		if (comment == null) return Helpers.notFoundRedirect(id, "comment");
		commentRepository.delete(comment);
		return "redirect:/task/show/" + comment.getTask().getId();
	}

	public void taskShowInit(Model model, Task task){

		Authentication principal = SecurityContextHolder.getContext().getAuthentication();
		// tell the model if the user is logged and is task's project's manager
		if(principal!=null){
			if(task.getProject().hasManager(principal.getName()) || principal.getAuthorities().contains(Authority.MANAGE)
					|| principal.getAuthorities().contains(Authority.MANAGE) ) {

				model.addAttribute("isManager", true);
				model.addAttribute("statuses", TaskStatus.values());
			}

			if(task.hasAssignedMember(principal.getName())){
				model.addAttribute("isAssignedToPrincipal", true);
				model.addAttribute("statuses", TaskStatus.values());
			}
		} else model.addAttribute("isManager", false);

		model.addAttribute("members", memberRepository.findAll());
		model.addAttribute("task", task);
	}

	private void validateTaskEditor(Task task) throws AccessDeniedException{
		if (task==null) throw new AccessDeniedException();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth==null) throw new AccessDeniedException();

		//if the principal is NOT project manager or admin or manager, throw exception
		if(! (task.getProject().hasManager(auth.getName()) || auth.getAuthorities().contains(Authority.ADMIN)  ||
				auth.getAuthorities().contains(Authority.MANAGE))){
			throw new AccessDeniedException();
		}
	}
}

