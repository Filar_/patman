package com.filar.patman.web.helpers;

import com.filar.patman.data.entities.Member;
import com.filar.patman.data.helpers.TaskStatus;
import org.springframework.ui.Model;

/**
 * Created by kamil on 23.03.16.
 */
public class Helpers {
	public static String notFoundRedirect (Long id, String type){
		return "redirect:/notfound?id=" + id + "&type=" + type.substring(0,1).toUpperCase() + type.substring(1);
	}
	public static void fillModelWithMembersTaskLists(Model model, Member member){
		model.addAttribute("tasksSize", member.getAssignedTasks().size());
			model.addAttribute("newTasks", member.getTasksWithStatus(TaskStatus.NEW));
			model.addAttribute("suspendedTasks", member.getTasksWithStatus(TaskStatus.SUSPENDED));
			model.addAttribute("inProgressTasks", member.getTasksWithStatus(TaskStatus.IN_PROGRESS));
			model.addAttribute("solvedTasks", member.getTasksWithStatus(TaskStatus.SOLVED));
			model.addAttribute("member", member);
	}
}
