<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${inProgressTasks.size() > 0}">
	<h4>In progress:</h4>
	<ul class="list-group">
		<c:forEach items="${inProgressTasks}" var="task">
			<li class="list-group-item"><a href="<c:url value="/task/show/${task.id}"/>"><c:out value="${task.name}"/></a></li>
		</c:forEach>
	</ul>
</c:if>

<c:if test="${newTasks.size() > 0}">
	<h4>New:</h4>
	<ul class="list-group">
		<c:forEach items="${newTasks}" var="task">
			<li class="list-group-item"><a href="<c:url value="/task/show/${task.id}"/>"><c:out value="${task.name}"/></a></li>
		</c:forEach>
	</ul>
</c:if>

<c:if test="${solvedTasks.size() > 0}">
	<h4>Solved:</h4>
	<ul class="list-group">
		<c:forEach items="${solvedTasks}" var="task">
			<li class="list-group-item"><a href="<c:url value="/task/show/${task.id}"/>"><c:out value="${task.name}"/></a></li>
		</c:forEach>
	</ul>
</c:if>

<c:if test="${suspendedTasks.size() > 0}">
	<h4>Suspended:</h4>
	<ul class="list-group">
		<c:forEach items="${suspendedTasks}" var="task">
			<li class="list-group-item"><a href="<c:url value="/task/show/${task.id}"/>"><c:out value="${task.name}"/></a></li>
		</c:forEach>
	</ul>
</c:if>