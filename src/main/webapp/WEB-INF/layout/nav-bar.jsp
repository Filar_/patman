<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="container">
	<nav role="navigation" class="navbar navbar-default">
		<div class="navbar-header">
			<button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>
		<div id="navbarCollapse" class="collapse navbar-collapse">
			<ul class="nav navbar-nav">
				<li><a href="<c:url value="/"/>">Home</a></li>
				<li><a href="<c:url value="/project/list"/>">Projects</a></li>
				<li><a href="<c:url value="/member/list"/>">Members</a></li>
			</ul>

			<ul class="nav navbar-nav navbar-right">
				<sec:authorize access="!isAuthenticated()">
					<li>
						<a href="<c:url value="/login"/>">Login</a>
					</li>
					<li>
						<a href="<c:url value="/member/register"/>">Register</a>
					</li>
				</sec:authorize>
				<sec:authorize access="isAuthenticated()">
					<li>
						<button type="submit" form="logout" class="btn btn-link navbar-btn">Logout</button>
					</li>
				</sec:authorize>
			</ul>
		</div>
	</nav>
</div>


<form method="POST" action="<c:url value="/logout"/>" id="logout">
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
</form>
