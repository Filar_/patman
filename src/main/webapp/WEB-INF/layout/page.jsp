<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
	<head>
		<meta charset="utf-8">
		<title> <tiles:insertAttribute name="page_title"/> </title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css">
	</head>
	<body>
		<div id="nav-bar">
			<tiles:insertAttribute name="nav-bar"/>
		</div>
		<div id="content">
			<div class="container">
				<tiles:insertAttribute name="body"/>
			</div>
		</div>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
            <script type="text/javascript" src="http://ciasteczka.eu/cookiesEU-latest.min.js"></script>
            <script type="text/javascript">

            jQuery(document).ready(function(){
            	jQuery.fn.cookiesEU();
            });

            </script>
	</body>
</html>