<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>


	<div class="row">
		<div class="col-sm-12">
			<sec:authorize access="!isAuthenticated()">
				<div class="jumbotron">
					<h2>Welcome to Patman!</h2>
					<p>Patman is a project and task management application.</p>
					<p>Please login or register to get started!</p>
				</div>
			</sec:authorize>
		</div>
	</div>
	<sec:authorize access="isAuthenticated()">
		<div class="well">
			<div class="row">
					<h2 style="margin-bottom: 2em;" class="text-center"> Hello, <em><c:out value="${member.fullName}"/></em>!</h2>

					<div class="col-sm-6">
						<c:choose>
							<c:when test="${tasksSize > 0}">
								<h3> Your tasks: </h3>
								<div class="row">
									<div class="col-xs-10">
										<tiles:insertAttribute name="tasks"/>
									</div>
								</div>
							</c:when>
							<c:otherwise>
								<h3><c:out value="You don't have any assigned tasks yet!"/></h3>
							</c:otherwise>
						</c:choose>
					</div>

					<div class="col-sm-6">
						<c:if test="${member.managedProjects.size() > 0}">
							<h3>Projects managed by you:</h3>
							<div class="row">
								<div class="col-xs-10">
									<ul class="list-group">
										<c:forEach items="${member.managedProjects}" var="project">
											<li class="list-group-item"><a href="<c:url value="/project/show/${project.id}"/>"><c:out value="${project.name}"/></a></li>
										</c:forEach>
									</ul>
								</div>
								<div class="col-xs-2"></div>
							</div>
						</c:if>
					</div>
			</div>
		</div>	
	</sec:authorize>

