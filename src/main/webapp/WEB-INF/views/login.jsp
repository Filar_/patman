<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="well">
	<h1 class="text-center">Login</h1>
	<div class="row">
		<div class="col-sm-4"></div>
		<div class="col-sm-4">
			<c:if test="${hasError == true}">
				<span class="text-danger">Invalid e-mail or password</span>
			</c:if>
			<form name='f' action='<c:url value="/login"/>' method='POST'>
				<div class="form-group">
					<label for="inputUsername">
						E-mail
					</label>
					<input type="text" name="username" id="inputUsername" class="form-control"/>
				</div>
				<div class="form-group">
					<label for="inputPassword">
						Password
					</label>
					<input type="password" name="password" id="inputPassword" class="form-control"/>
				</div>

				<div class="checkbox">
					<label>
							<input type="checkbox" name="remember-me"/>
							Remember me
					</label>
				</div>
				<button type="submit" class="btn btn-primary">Login</button>
				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
			</form>
		</div>
		<div class="col-sm-4"></div>
	</div>
</div>
