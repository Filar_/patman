<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<div class="well">
	<h1 class="text-center"> Member list </h1>
	<div class="row" style="margin-top: 2em;">
		<div class="col-sm-12">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>
							Name
						</th>
						<th>
							E-mail
						</th>
						<th>
							Active tasks
						</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${members}" var="member">
						<tr>
							<td>
								<a href="<c:url value="/member/show/${member.id}"/>">
									<c:out value="${member.fullName}"/>
								</a>
							</td>
							<td>
								<c:out value="${member.email}"/>
							</td>
							<td>
								${member.numberOfActiveTasks}
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</div>