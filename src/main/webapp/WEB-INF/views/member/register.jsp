<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
	<div class="well">
		<div class="row">
			<div class="col-md-4 col-sm-3"></div>
			<div class="col-md-4 col-sm-6">
				<h1 class="text-center">Register</h1>
				<sf:form method="POST" commandName="newMember">
					<div style="margin-bottom:0.5em; margin-top:1em;">
						<div class="text-danger" >
							${passwordsMatch ? "" : "Passwords don't match"}
						</div>
						<sf:errors path="*" element="div" cssClass="text-danger"/>
					</div>
					<spring:bind path="firstName">
						<div class="form-group ${status.error ? 'has-error' : ''}">
							<sf:label path="firstName">First name:</sf:label>
							<sf:input path="firstName" class="form-control  "/>
						</div>
					</spring:bind>

					<spring:bind path="surname">
						<div class="form-group ${status.error ? 'has-error' : ''}">
							<sf:label path="surname">Surname:</sf:label>
							<sf:input path="surname" class="form-control has-error"/>
						</div>
					</spring:bind>

					<spring:bind path="email">
						<div class="form-group ${status.error ? 'has-error' : ''}">
							<sf:label path="email">E-mail:</sf:label>
							<sf:input path="email" type="email" class="form-control"/>
						</div>
					</spring:bind>

					<%--
							regular html input is used for password, it will
							be saved as model attribute to be compared with the retyped password
					--%>
					<spring:bind path="password">
						<div class="form-group ${status.error ? 'has-error' : ''}">
							<sf:label path="password">Password:</sf:label>
							<sf:password path="password" class="form-control"/>
						</div>
					</spring:bind>
					<div class="form-group ${passwordsMatch ? '' : 'has-error'}">
						<label id="inputRetypePassword">Retype password:</label>
						<input id="inputRetypePassword" type="password" name="retypedPassword" class="form-control"/>
					</div>
					<input type="submit" value="Register" class="btn btn-primary"/>
				</sf:form>
			</div>
			<div class="col-md-4 col-sm-3"></div>
		</div>
	</div>
