<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

	<div class="well">
		<div class="row">
			<div class="col-sm-3">
				<h1><c:out value="${member.fullName}"/></h1>
				<c:forEach items="${member.authorities}" var="auth">
					<div>
						<label class="label label-info">
							${auth.friendlyName}
						</label>
					</div>
				</c:forEach>
				<div style="margin-top: 2em;">
					<c:out value="${member.email}"/>
				</div>
			</div>
			<div class="col-sm-9">
				<div class="row">
					<div class="col-sm-8 col-xs-10">
						<c:choose>
							<c:when test="${member.assignedTasks.size()==0 && member.managedProjects.size()==0}">
								<h3>No tasks have been assigned to this member yet</h3>
							</c:when>
							<c:otherwise>
								<h2> Tasks: </h2>
								<tiles:insertAttribute name="tasks"/>
							</c:otherwise>
						</c:choose>

						<c:if test="${member.managedProjects.size() > 0}">
							<h2 style="margin-top:2em;"> Managed Projects: </h2>
							<div class="panel panel-default">
								<div class="panel-body">
									<table class="table table-hover">
										<thead>
											<tr>
												<th>Name</th>
												<th>Number of tasks</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach items="${member.managedProjects}" var="project">
												<tr>
													<td>
														<a href="<c:url value="/project/show/${project.id}"/>"><c:out value="${project.name}"/></a>
													</td>
													<td>
														${project.tasks.size()}
													</td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
							</div>
						</c:if>
					</div>
					<div class="col-sm-4 col-cs-2"></div>
				</div>
			</div>
		</div>
	</div>
