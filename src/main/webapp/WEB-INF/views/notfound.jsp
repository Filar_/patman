<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="jumbotron">
		<h1> 404 Not found </h1>
		<p><c:out value="${type} with ID ${id} doesn't exist."/></p>
		<a href="<c:url value="/"/>" class="btn btn-primary">Homepage</a>
</div>