<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf" %>

<div class="well">
	<div class="row">
		<h1 class="text-center">Manage project</h1>
		<div class="col-sm-4"></div>
		<div class="col-sm-4 col-xs-10">
			<sf:form method="POST" commandName="project" action="changename">
				<sf:errors path="*" element="div" cssClass="errors"/>
				<div class="form-group">
					<sf:label path="name">
						New name:
					</sf:label>
					<sf:input path="name" class="form-control"/>
					<button type="submit" class="btn btn-default">Save</button>
				</div>
				<sf:hidden path="id"/>
			</sf:form>

			<form method="POST" action="addmanager">
				<div class="form-group">
					<label for="inputManagerId">
						Add project manager:
					</label>
					<select name="managerId" id="inputManagerId" class="form-control">
						<c:forEach items="${members}" var="member">
							<option value="${member.id}"><c:out value="${member.fullName}"/></option>
						</c:forEach>
					</select>
					<button type="submit" class="btn btn-default">Add</button>
					<input type="hidden" name="projectId" value="${project.id}"/>
					<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
				</div>
			</form>

			Managers:
			<div>
				<c:if test="${project.managers.size() == 0}"> No managers set for this project!</c:if>
			</div>

			<c:forEach items="${project.managers}" var="manager">
				<div class="row">

					<form method="POST" action="deletemanager" style="margin-bottom: 0">
						<div class="col-xs-8">
							<a href="<c:url value="/member/show/${manager.id}"/>"><c:out value="${manager.firstName} ${manager.surname}"/></a href>
						</div>
						<div class="col-xs-4">
							<button type="submit" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-remove"></span></button>

						<input type="hidden" name="managerId" value="${manager.id}"/>
						<input type="hidden" name="projectId" value="${project.id}"/>
						<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
						</div>
					</form>
				</div>
			</c:forEach>
		</div>
		<div class="col-sm-4 col-xs-2"></div>
	</div>
</div>