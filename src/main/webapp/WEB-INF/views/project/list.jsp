<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<div class="well">
	<div class="row">
		<div class="col-sm-4"></div>
		<div class="col-sm-4">
			<h1 class="text-center">Projects:</h1>
		</div>
		<div class="col-sm-4">
			<sec:authorize access="isAuthenticated() and (hasAuthority('MANAGE') or hasAuthority('ADMIN'))">
				<a href="<c:url value="/project/new"/>" class="btn btn-default btn-lg pull-right">New</a>
			</sec:authorize>
		</div>
	</div>
	<table class="table table-hover" style="margin-top:2em;">
		<thead>
			<tr>
				<th>Name</th>
				<th>Number of tasks</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${projects}" var="project">
				<tr>
					<td>
						<a href="<c:url value="/project/show/${project.id}"/>"><c:out value="${project.name}"/></a>
					</td>
					<td>
						${project.tasks.size()}
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>