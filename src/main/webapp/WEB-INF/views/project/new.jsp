<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf" %>

<div class="well">
	<h1 class="text-center">New project</h1>
	<div class="row">
		<div class="col-sm-4"></div>
		<div class="col-sm-4">
			<sf:form method="POST" commandName="project">
				<sf:errors path="*" element="div" cssClass="text-danger"/>
					<div class="form-group">
						<sf:label path="name">
							Project name:
						</sf:label>
						<sf:input path="name" class="form-control"/>
					</div>
					<div class="form-group">
						<label for="inputManagerId">
							Initial manager:
						</label>
						<select name="managerId" id="inputManagerId" class="form-control">
							<c:forEach items="${memberList}" var="member">
								<c:choose>
									<c:when test="${member.id == managerId}">
										<option value="${member.id}" selected>${member.firstName} ${member.surname}</option>
									</c:when>
									<c:otherwise>
										<option value="${member.id}">${member.firstName} ${member.surname}</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</select>
					</div>
				<button type="submit" class="btn btn-default">Submit</button>
			</sf:form>
		</div>
		<div class="col-sm-4"></div>
	</div>
</div>