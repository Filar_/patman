<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>


	<div class="well" style="margin-bottom:2em;">
		<div class="row">
				<div class="col-xs-2"></div>
			<div class="col-xs-8">
				<h1 style="margin-bottom: 2em;">
					<div class="text-center"><c:out value="${project.name}"/></div>
			    </h1>
			</div>
			<sec:authorize access="isAuthenticated() and ( hasAuthority('MANAGE') or hasAuthority('ADMIN') )">
				<div class="col-xs-2">
						<a href="<c:url value="/project/edit/${project.id}"/>" class="btn btn-default btn-xs pull-right" style="margin-top:2em;">Manage</a>
				</div>
			</sec:authorize>
		</div>

		<div class="row">
			<div class="col-sm-6">
				<h2>Project managers:</h2>
				<c:if test="${project.managers.size() == 0}"> No managers set for this project!<br> </c:if>
				<ul class="list-group">
					<c:forEach items="${project.managers}" var="manager">
						<li class="list-group-item">
							<a href="<c:url value="/member/show/${manager.id}"/>"><c:out value="${manager.firstName} ${manager.surname}"/></a href><br>
						</li>
					</c:forEach>
				</ul>
			</div>

			<div class="col-sm-6">
				<h2>Tasks:</h2>
				<c:if test="${project.tasks.size() == 0}"> <div style="margin-bottom: 1.5em;"> <small><c:out value="This project doesn't have any tasks yet!"/></small> </div> </c:if>
				<ul class="list-group">
					<c:forEach items="${project.tasks}" var="task">
						<li class="list-group-item">
							<a href="<c:url value="/task/show/${task.id}"/>"><c:out value="${task.name}"/></a><br>
						</li>
					</c:forEach>
				</ul>
				<c:if test="${isManager == true}">
					<a href="<c:url value="/task/new?projectId=${project.id}"/>" class="btn btn-default btn-sm">New task</a>
				</c:if>
			</div>
		</div>
	</div>
</div>
