<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf" %>

<div class="well">
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-6">
			<sf:form method="POST" commandName="comment">
				<sf:errors path="*" cssClass="text-danger"/>
					<sf:label path="content">
						New content:
					</sf:label>
					<sf:textarea path="content" rows="6" class="form-control"/><br>
					<sf:hidden path="id"/>
					<button type="submit" class="btn btn-primary">Submit</button>
			</sf:form>
		</div>
		<div class="col-sm-3"></div>
	</div>
</div>
