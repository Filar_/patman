<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf" %>

<div class="well">
	<h1 class="text-center">Edit task</h1>
	<div class="row">
		<div class="col-sm-4"></div>
		<div class="col-sm-4">
			<sf:form method="POST" commandName="task">
				<div><sf:errors path="*" cssClass="text-danger"/></div>
				<div class="form-group">
					<sf:label path="content">
						New content:
					</sf:label>
					<sf:textarea path="content" rows="6" class="form-control"/>
					<button type="submit" class="btn btn-default">Save</button>
				</div>
				<sf:hidden path="id"/>
				<sf:hidden path="name"/>
			</sf:form>
		</div>
		<div class="col-sm-4"></div>
	<div>
</div>
