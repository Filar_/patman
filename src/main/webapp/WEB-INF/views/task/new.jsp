<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>                             

	<div class="well">
		<div class="row">
			<div class="col-md-4 col-sm-3"></div>
			<div class="col-md-4 col-sm-6">
				<div class="text-center">
					<h1 style="margin-bottom:1em;">New task:</h1>
				</div>
				<sf:form method="POST" commandName="newTask">
					<sf:errors path="*" element="div" cssClass="text-danger" style="margin-bottom:1em;" />
					<div class="form-group">
						<sf:label path="name" cssErrorClass="text-danger">
							Name
						</sf:label>
						<spring:bind path="name">
							<div class=" ${status.error ? 'has-error' : '' }">
								<sf:input path="name" class="form-control"/>
							</div>
						</spring:bind>
					</div>
					<div class="form-group">
						<sf:label path="content" cssErrorClass="text-danger">
							Content
						</sf:label>
						<spring:bind path="content">
							<div class=" ${status.error ? 'has-error' : '' }">
								<sf:textarea path="content" rows="5" class="form-control" placeholder="task content"/>
							</div>
						</spring:bind>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-default">Create</button>
					</div>
					<input type="hidden" name="projectId" value="${projectId}"/>
				</sf:form>
			</div>
			<div class="col-md-4 col-sm-3"></div>
		</div>
	</div>
