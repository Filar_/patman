<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf" %>
<div class="jumbotron">
	<h1>No projects</h1>
	<p>
		<c:out value="You can't create a task when no projects are available!"/>
	</p>
	<p>
		<c:out value="Please first create a project, then add a task to it"/>
	</p>
	<a href="<c:url value="/project/new"/>" class="btn btn-default">New project</a>
</div>