<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>


	<div class="well">
		<h1><c:out value="${task.name}"/></h1>
		<div class="row">
			<div class="col-md-4">
				<p>Project: <a href="<c:url value="/project/show/${task.project.id}"/>"><c:out value="${task.project.name}"/></a></p>
				<p>Author: <a href="<c:url value="/member/show/${task.author.id}"/>"><c:out value="${task.author.fullName}"/></a></p>
				<p>
					<div class="row">

						<div class="col-xs-2 col-sm-1 col-md-2">
							Status:
						</div>
						<div class="col-xs-2 col-sm-1 col-md-3">
							<div class="label label-info">
								<c:out value="${task.status.friendlyName}"/>
							</div>
						</div>

						<div class="col-xs-8 col-sm-10 col-md-7">
							<c:if test="${ isAssignedToPrincipal == true || isManager == true }">
								<div class="btn-group">
									<button type="button" data-toggle="dropdown" class="btn btn-default btn-xs dropdown-toggle">Change <span class="caret"></span></button>
									<ul class="dropdown-menu">
										<c:forEach items="${statuses}" var="status">
										<c:if test="${status.name().equals(task.status.name())== false}">
												<li>
													<form method="POST" action="<c:url value="/task/changestatus"/>" style="padding:0; margin:0;" >
														<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
														<input type="hidden" name="taskId" value="${task.id}"/>
														<input type="hidden" name="memberId" value="${member.id}"/>
														<input type="hidden" name="newStatus" value="${status.name()}"/>
														<button type="submit" class="btn btn-block">${status.friendlyName}</button>
													</form>
												</li>
											</c:if>
										</c:forEach>
									</ul>
								</div>
							</c:if>
						</div>
					</div>
				</p>

				<div>
					Assigned members:<br>
					<c:if test="${task.assignedTo.size()==0}">
						<span style="font-style: italic">none</span>
					</c:if>
				</div>


				<c:forEach items="${task.assignedTo}" var="member">
					<div class="row">
						<div class="col-xs-5">
							<a href="<c:url value="/member/show/${member.id}"/>"><c:out value="${member.fullName}"/></a>
						</div>
						<div class="col-xs-7">
							<c:if test="${isManager==true}">
								<form style="display: inline-block" method="POST" action="<c:url value="/task/dissociate"/>">
									<button type="submit" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-remove"></span></button>
									<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
									<input type="hidden" name="taskId" value="${task.id}"/>
									<input type="hidden" name="memberId" value="${member.id}"/>
								</form>
							</c:if>
						</div>
					</div>
				</c:forEach>

				<div>
					<c:if test="${isManager==true}">
						<form method="POST" action="<c:url value="/task/assign"/>">
							<label id="assignTo">
								Assign to:
							</label>
							<div class="row">
								<div class="col-md-7">
									<select name="memberId" id="assignTo" class="form-control input-sm">
										<c:forEach items="${members}" var="member">
											<option value="${member.id}"><c:out value="${member.fullName}"/></option>
										</c:forEach>
									</select>
								</div>
								<div class="col-md-5">
									<input type="submit" value="Assign" class="btn btn-default btn-sm"/>
									<input type="hidden" name="taskId" value="${task.id}"/>
									<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
								</div>
							</div>
						</form>
					</c:if>
				</div>


			</div>
			<div class="col-md-8">
				<div class="row">
					<div class="col-md-9">
						<div class="panel panel-primary" style="margin-top:1em;">
							<div class="panel-heading">
								Content
								<c:if test="${isManager}">
									<a href="<c:url value="/task/edit/${task.id}"/>" class="btn btn-info btn-xs pull-right">Edit</a>
								</c:if>
							</div>
							<div class="panel-body" style="word-wrap: break-word">
								<c:out value="${task.content}"/>
							</div>
						</div>
					</div>
					<div class="col-md-3"></div>
				</div>

				<div>
					<h2 style="margin-bottom:1em; margin-top:1em;">History <span class="label label-info">${task.events.size()}</span></h2>
					<div class="row">
						<div class="col-sm-9">
							<c:forEach items="${task.events}" var="event">
								<h3>
									<div class="row">
										<div class="col-sm-9">
											<a href="<c:url value="/member/show/${event.author.id}"/>"><c:out value="${event.author.fullName}"/></a>
											<small> ${event.date} </small>
										</div>
										<div class="col-sm-3">
											<div class="pull-right">
												<c:if test="${event.isEditable()==true}">
													<sec:authorize access="isAuthenticated() and ( hasAuthority('MANAGE') or hasAuthority('ADMIN') )">
														<div class="btn-group">
															<a href="<c:url value="/task/comment/edit/${event.id}"/>" class="btn btn-default btn-xs">Edit</a>
															<button type="submit" form="deleteForm" class="btn btn-default btn-xs">Delete</button>

														</div>
														<form method="POST" style="margin:0;" id="deleteForm" action="<c:url value="/task/comment/delete/${event.id}"/>">
															<input type="hidden" name="id" value="${event.id}"/>
															<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
														</form>
													</sec:authorize>
												</c:if>
											</div>
										</div>
									</div>
								</h3>
								<div class="panel panel-info">
									<div class="panel-body" style="word-wrap: break-word;">
										<c:out value="${event.content}"/>
									</div>
								</div>
							</c:forEach>


							<sec:authorize access="isAuthenticated()">
								<sf:form method="POST" style="margin-top:3em;" servletRelativeAction="/task/comment/new" commandName="newComment">
									<sf:errors path="content" cssClass="text-danger"/>
									<div class="form-group">
										<sf:label path="content">Add coment:</sf:label>
										<sf:textarea class="form-control" path="content" rows="6" cols="35"/><br>
										<input type="submit" value="Submit" class="btn btn-default pull-right"/>
									</div>
									<input type="hidden" name="taskId" value="${task.id}"/>
								</sf:form>
							</sec:authorize>

						</div>
						<div class="col-sm-3"></div>
					</div>
				</div>


			</div>
		</div>
	</div>
