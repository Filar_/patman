package com.filar.patman.data.entities;

import com.filar.patman.data.entities.task.Task;
import com.filar.patman.data.helpers.TaskStatus;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by kamil on 20.03.16.
 */
public class MemberTest {
	Member member = new Member();

	static final int NEW_TASKS = 2;
	static final int SUSPENDED_TASKS = 1;
	static final int SOLVED_TASKS = 0;

	public MemberTest(){
		member.assignTask(new Task(1L, "NEW1", "1", TaskStatus.NEW, null, null));
		member.assignTask(new Task(2L, "NEW2", "2", TaskStatus.NEW, null, null));
		member.assignTask(new Task(3L, "SUSPENDED1", "1", TaskStatus.SUSPENDED, null, null));
	}

	@Test
	public void shouldReturnNumberOfTasksWithStatus(){
		assertEquals(NEW_TASKS, member.getTasksWithStatus(TaskStatus.NEW).size());
		assertEquals(SUSPENDED_TASKS, member.getTasksWithStatus(TaskStatus.SUSPENDED).size());
		assertEquals(SOLVED_TASKS, member.getTasksWithStatus(TaskStatus.SOLVED).size());
	}

}
