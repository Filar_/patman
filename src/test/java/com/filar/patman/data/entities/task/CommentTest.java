package com.filar.patman.data.entities.task;

import com.filar.patman.data.entities.Member;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Date;

/**
 * Created by kamil on 30.03.16.
 */

public class CommentTest {
	private Date now = new Date();
	private Member author = Mockito.mock(Member.class);
	private Task task = Mockito.mock(Task.class);
	private Comment comment1 = new Comment(null, now, author, "123", task);
	private Comment comment2 = new Comment(null, now, author, "123", task);
	private Comment differentComment = new Comment(null, now, author, "dududu", task);
	private Comment nullAuthor = new Comment(null, now, null, "123", task);
	private Comment nullAuthor2 = new Comment(null, now, null, "123", task);
	@Test
	public void hashCodeShouldBeEqualWithTwoSameComments(){
		Assert.assertTrue(comment1.hashCode() == comment2.hashCode());
	}
	@Test
	public void hashCodeShouldBeEqualWithTwoDifferentComments(){
		Assert.assertFalse(comment1.hashCode() == differentComment.hashCode());
		Assert.assertFalse(comment2.hashCode() == differentComment.hashCode());
	}
	@Test
	public void equalsShouldReturnTrueWithTwoSameComments(){
		Assert.assertTrue(comment1.equals(comment2));
		Assert.assertTrue(comment2.equals(comment1));
		Assert.assertTrue(nullAuthor.equals(nullAuthor2));
	}
	@Test
	public void equalsShouldReturnFalseWithTwoDifferentComments(){
		Assert.assertFalse(comment1.equals(differentComment));
		Assert.assertFalse(differentComment.equals(comment2));
	}
	@Test
	public void equalsShouldReturnFalseWithDifferentCommentsOneWithNullAuthor(){
		Assert.assertFalse(comment1.equals(nullAuthor));
	}

	@Test
	public void equalsShouldReturnTrueWithTwoSameTestsWithNullAuthor(){
		Assert.assertTrue(nullAuthor.equals(nullAuthor2));
	}

}
