package com.filar.patman.data.entities.task;

import com.filar.patman.data.entities.Member;
import com.filar.patman.data.entities.task.Task;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.Assert;

import static org.junit.Assert.*;

/**
 * Created by kamil on 19.03.16.
 */
@RunWith(JUnitParamsRunner.class)
public class TaskTest {
	private Task task;
	private static final String email[]={"kowalski@gmail.com", "john@john.com"};

	private static final Object[] getEmails(){
		return email;
	}

	private static final Object[] getWrongEmails(){
		return new String[] {"notrealemail@com.com", "anothernotrealemail@email.pl", "thisisnotevenanemail"};
	}

	@Before
	public void init(){
		task = new Task();
		task.assignMember(new Member( 2L, "Jan", "Kowalski", "43121", email[0]));
		task.assignMember(new Member( 1L, "John", "Smith", "1234", email[1]));
	}

	@Test
	@Parameters(method="getEmails")
	public void taskShouldContainMemberWithEmail(String email){
		assertTrue(task.hasAssignedMember(email));
	}

	@Test
	@Parameters(method = "getWrongEmails")
	public void taskShouldNotFindMembersWithFakeEmails(String email){
		assertFalse(task.hasAssignedMember(email));
	}

}
